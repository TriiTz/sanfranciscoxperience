               
<!-- Please help me Aliens are after me, I managed to lose them in San Francisco but they found me again please help me><!DOCTYPE html -->
<html>
<title>San Francisco Experience</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<style>
body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
</style>
<body class="w3-light-blue">

<!-- w3-content defines a container for fixed size centered content, 
and is wrapped around the whole page content, except for the footer in this example -->
<div class="w3-content" style="max-width:1400px">

<!-- Header -->
<header class="w3-container w3-center w3-padding-32"> 
  <h1><b>Tristan's San Francisco Experience</b></h1>
  <p>Welcome to my website <span class="w3-tag w3-blue">weirdo</span></p>
</header>

<!-- Grid -->
<div class="w3-row">

<!-- Blog entries -->
<div class="w3-col l8 s12">
  <!-- Blog entry -->
  <div class="w3-card-4 w3-margin w3-white">
    <img src="images/trist.jpg" alt="K-mart" style="width:100%">
    <div class="w3-container">
      <h3><b>Overall</b></h3>
      <h5>Quick Summary, <span class="w3-opacity">Day 64 after they came, What year is this ?!</span></h5>
    </div>
<!-- Please help me Aliens are after me, I managed to lose them in San Francisco but they found me again please help me><!DOCTYPE html -->
    <div class="w3-container">
      <p>San Francisco was a great experience even though it was cut short. I was thourougly impressed by the cityscape at night and the surprising sightseeing places such as Twin Peaks or Bernal Heights which were a welcome dichotomy from the urban feeling the rest of the city gave out. San Fransiscans played a big role in this experience, they were warm and open to any conversation one would throw at them, they were also sometimes fun and crazy, sometimes a bit too much...</p>
      <div class="w3-row">
        <div class="w3-col m8 s12">
          <p><button class="w3-button w3-padding-large w3-white w3-border"><b>READ MORE »</b></button></p>
        </div>
        <div class="w3-col m4 w3-hide-small">
          <p><span class="w3-padding-large w3-right"><b>Comments : </b> <span class="w3-tag w3-light-grey">A ton! People seem super interested</span></span></p>
        </div>
      </div>
    </div>
  </div>
  <hr>
<!-- Please help me Aliens are after me, I managed to lose them in San Francisco but they found me again please help me><!DOCTYPE html -->
  <!-- Blog entry -->
  <div class="w3-card-4 w3-margin w3-white">
  <img src="images/sf.jpg" alt="Cityscape" style="width:100%">
    <div class="w3-container">
      <h3><b>BUSINESS...</b></h3>
      <h5>       ...as usual... <span class="w3-opacity">40 days before it happened</span></h5>
    </div>

    <div class="w3-container">
      <p>Business as expected was a core part of the San Francisco culture. Networking there is made easy, from conferences, networking events, meetups, hackathons to the open-mindedness of the people. After a couple weeks there I was already on my way to a private marketing pitch in Palo Alto. A product of a medical nature was about to launch, there I spoke to the medical specialist there who served as a referral. Jump of few weeks from then and I was recruited to help that same person to Lauch her own products. </p>
      <div class="w3-row">
        <div class="w3-col m8 s12">
          <p><button class="w3-button w3-padding-large w3-white w3-border"><b>READ MORE »</b></button></p>
        </div>
        <div class="w3-col m4 w3-hide-small">
          <p><span class="w3-padding-large w3-right"><b>Comments  </b> <span class="w3-badge w3-light-grey">∞</span></span></p>
        </div>
      </div>
    </div>
  </div>
  <hr>

   <div class="w3-card-4 w3-margin w3-white">
  <img src="images/haight.jpg" alt="Cityscape" style="width:100%">
    <div class="w3-container">
      <h3><b>Thrift Stores</b></h3>
      <h5> Fashion at the right price <span class="w3-opacity">15 days before it happened</span></h5>
    </div>

    <div class="w3-container">
      <p> It seems that the thrift feaver has not yet reached San Francisco. In the hyppie neighborhood that is Haight Street, thrift stores make up a lot of the stores. Well organized, with a great and large selection, the shops there could easily rival with any Parisian thrift shop. However, their prices were impressively low. It seemed even as they were sometimes struggling to sell. </p>
      <div class="w3-row">
        <div class="w3-col m8 s12">
          <p><button class="w3-button w3-padding-large w3-white w3-border"><b>READ MORE »</b></button></p>
        </div>
        <div class="w3-col m4 w3-hide-small">
          <p><span class="w3-padding-large w3-right"><b>Comments  </b> <span class="w3-badge w3-light-grey">∞</span></span></p>
        </div>
      </div>
    </div>
  </div>
<!-- END BLOG ENTRIES -->
</div>
<!-- Please help me Aliens are after me, I managed to lose them in San Francisco but they found me again please help me><!DOCTYPE html -->
<!-- Introduction menu -->
<div class="w3-col l4">
  <!-- About Card -->
  <div class="w3-card w3-margin w3-margin-top">
  <img src="images/avatar.jpg" alt="way_too_close" style="width:100%">
    <div class="w3-container w3-white">
      <h4><b>Yours truly</b></h4>
      <p>Although ending up here was only the result of an abduction, I found myself enjoying San Francisco a lot. From food to viewscapes and of course business opportunities. San Francisco was without contest the best outcome imaginable for an abduction. </p>
    </div>
  </div><hr>
  
  <!-- Posts -->
  <div class="w3-card w3-margin w3-blue" >
    <div class="w3-container w3-padding">
      <h4>Things I liked!</h4>
    </div>
    <ul class="w3-ul w3-hoverable w3-white">
      <li class="w3-padding-16">
        <img src="images/yosemite.jpg" alt="Image" class="w3-left w3-margin-right" style="width:50px">
        <span class="w3-large">Yosemite</span><br>
        <span>Went to Yosemite, one of the most beautiful places in California, a little crowded though</span>
      </li>
      <li class="w3-padding-16">
        <img src="images/la.jpg" alt="Image" class="w3-left w3-margin-right" style="width:50px">
        <span class="w3-large">Los Angeles</span><br>
        <span>It wouldn't be a good trip to San Francisco without... Los Angeles ?</span>
      </li> 
      <li class="w3-padding-16">
        <img src="images/dimsum.jpg" alt="Image" class="w3-left w3-margin-right" style="width:50px">
        <span class="w3-large">Lorem Dim Sum</span><br>
        <span>With places like Good Mong Kok lost in the middle of ChinaTown, Dim Sum fans were up for a treat. And if you don't like Dim Sums, their noodles cost only 2,50$ and could feed a family</span>
      </li>   
      <li class="w3-padding-16 w3-hide-medium w3-hide-small">
        <img src="images/alien.jpg" alt="Image" class="w3-left w3-margin-right" style="width:50px">
        <span class="w3-large">No aliens!</span><br>
        <span>When planning a trip we often ask oursleves : "Is it nice for the kids ? How's the criminality rate overthere ?" And even if the answer to both these questions is respectively "NO!" and "High", at least I didn't see many Aliens there</span>
      </li>  
    </ul>
  </div>
  <hr> 
 <!-- Please help me Aliens are after me, I managed to lose them in San Francisco but they found me again please help me><!DOCTYPE html -->
  <!-- Labels / tags -->
  <div class="w3-card w3-margin w3-blue">
    <div class="w3-container w3-padding">
      <h4>Tags</h4>
    </div>
    <div class="w3-container w3-white">
    <p><span class="w3-tag w3-light-grey w3-small w3-margin-bottom">San Francisco</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Dim Sum</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Los Angeles</span>
      <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Universal Studios</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Classes</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Being under 21</span>
      <span class="w3-tag w3-blue w3-small w3-margin-bottom">They</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Thrift</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Family</span>
      <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">News</span> <span class="w3-tag w3-blue w3-small w3-margin-bottom">are</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Shopping</span>
      <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Business</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Networking</span> 
      <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">afterwork</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">coding</span>

      <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">sightseeing</span> <span class="w3-tag w3-blue w3-small w3-margin-bottom">after</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Ideas</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Clothing</span> <span class="w3-tag w3-blue w3-small w3-margin-bottom">me</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Yosemite</span>
    </p>
    </div>
  </div>
  
<!-- END Introduction Menu -->
</div>

<!-- END GRID -->
</div><br>

<!-- END w3-content -->
</div>

<!-- Footer -->
<footer class="w3-container w3-blue w3-padding-32 w3-margin-top">
  <button class="w3-button w3-black w3-disabled w3-padding-large w3-margin-bottom">Click to send help</button>
  <button class="w3-button w3-dark-grey w3-padding-large w3-margin-bottom">Next »</button>
  <p>Powered by <a href="https://geekprank.com/matrix-code-rain/" target="_blank">They are onto us</a></p>
</footer>

</body>
</html>

<!-- Please help me Aliens are after me, I managed to lose them in San Francisco but they found me again please help me>
